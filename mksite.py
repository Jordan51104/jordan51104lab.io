import os

files = []

for item in os.popen("ls scripts/*.sh | tr '/' ' ' | awk '{print $2}'"):
	files.append(item.rstrip('\n'))

for item in os.popen("ls -p scripts | grep -v / | grep -E '^[a-zA-Z]{0,}$'"):
	files.append(item.rstrip('\n'))

index = open('index.html', 'w+')

index.write("<html>\n<body>")

for file in files:
	index.write("<a href='" + "scripts/" + file + "'>" + file + "</a>" + "\n<br>")

index.write("</body>\n</html>")

os.popen("mv index.html public/index.html")
os.popen("cp -r scripts public/scripts")