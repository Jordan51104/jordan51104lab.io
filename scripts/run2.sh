#/bin/bash
RED='\033[0;31m'
GRN='\033[0;32m'
NC='\033[0m' # No Color

printf "${RED}This is the second file you should run. Running this file first will probably screw something up.${NC}"

printf "`ls /usr/share/zoneinfo/`\nPlease select your region (you can scroll up with shift+pageup):\n"
read region

printf "`ls /usr/share/zoneinfo/$region`\nPlease select your city:\n"
read city

ln -sf /usr/share/zoneinfo/"$region"/"$city" /etc/localtime

hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen

locale-gen

touch /etc/hostname
printf "What do you want your hostname to be?:\n"
read host

echo "$host" >> /etc/hostname

echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       myhostname.localdomain myhostname" >> /etc/hosts

mkinitcpio -p linux

grub-install --target=i386-pc /dev/"$disk"

grub-mkconfig -o /boot/grub/grub.cfg

echo "Now you have to set your root password. Just run 'passwd' and press enter. Then type your password."
echo "Then, you have to type 'exit' and then 'reboot'. Then you can remove the flashdrive and you are done!"
